# Intelligent Search

An example project on implementing Intelligent Search with the gem [Searchkick](https://github.com/ankane/searchkick) in a Rails app

![IntelligentSearch](https://gitlab.com/ortegacmanuel/global-sloppy-search/raw/master/docs/intelligent_search_searchkick.png)
